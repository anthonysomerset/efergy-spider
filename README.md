# Efergy spider

PHP class that connects to http://genage.efergy.com and allows you to query for the real time or the last 24 hours readings

## Disclaimer

Please note this code is only intended for educational purposes. 

It is not backed by Efergy in any way, so it will probably break if they change their unpublished API (see point 7 in their Terms & Conditions).

Be aware that Efergy might ban or close your account due to improper usage (see point 11 in their Terms & Conditions).

I am not liable for any consecuence derived from the use of this code.

## Usage

Rename config-sample.php to config.php and edit its content.

Run the example code.

